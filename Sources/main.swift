
private let arguments = CommandLine.arguments

public var verboseLoggingEnabled = false

for (index, argument)  in arguments.enumerated() {
    if index == 1 && argument == "logEnabled" {
        verboseLoggingEnabled = true
    }
}

let elevatorController = ElevatorControllerBuilder().numberOfFloors(floors: 6).build()

try! elevatorController.request(floor: 5)
try! elevatorController.destination(floor: 2)
print("Current floor \(elevatorController.currentFloor)")
try!elevatorController.request(floor: 3)
print("Current floor \(elevatorController.currentFloor)")
try! elevatorController.destination(floor: 4)
try! elevatorController.request(floor: 1)
print("Current floor \(elevatorController.currentFloor)")

