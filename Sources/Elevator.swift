//
//  Elevator.swift
//  Elevator
//
//  Created by Rohit Kotian on 2/25/17.
//
//
import Queue

enum ElevatorState {
    case idle, moving, busy
}

enum Direction {
    case up, down, stay
}

class Elevator {
    
    // Variables
    var state: ElevatorState = .idle
    var currentFloor: Int = 1
    var destinations = Queue<Int>()
    
    // MARK: - Initializer
    
    init() { }
    
}

// MARK: - Public Methods

extension Elevator {

    func move(_ to: Int) {
        if verboseLoggingEnabled {
            print("moving to \(to)")
        }
        let direction = _moveDirection(to)
        while currentFloor != to {
            _move(direction)
        }
        _open()
    }

    func request(_ destination: Int) {
        destinations.enqueue(destination)
    }
    
}


// MARK: - Private Helper Methods

fileprivate extension Elevator {
    
    fileprivate func _open() {
        if verboseLoggingEnabled {
            print("opening at \(currentFloor)")
        }
        state = .busy
        _close()
    }
    
    fileprivate func _close() {
        state = .idle
        if verboseLoggingEnabled {
            print("closing at \(currentFloor)")
        }
        if let next = destinations.dequeue() {
            move(next)
        }
    }
    
    fileprivate func _move(_ direction: Direction) {
        state = .moving
        if direction == .up {
            currentFloor += 1
        } else {
            currentFloor -= 1
        }
    }
    
    fileprivate func _moveDirection(_ floor: Int) -> Direction {
        if floor == currentFloor {
            return .stay
        }
        let floorDifference = currentFloor - floor
        if floorDifference < 0 {
            return .up
        } else {
            return .down
        }
    }
}
