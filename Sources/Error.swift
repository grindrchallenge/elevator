//
//  Error.swift
//  Elevator
//
//  Created by Rohit Kotian on 2/25/17.
//
//

import Foundation

private let elevatorErrorDomain = "com.elevator.error"

enum ElvatorError {
    
    case invalidRequest
    case invalidDestination
    
    var code: Int {
        switch self {
        case .invalidRequest:
            return 66
        case .invalidDestination:
            return 67
        }
    }
    
    /*
     NSLocalizedDescriptionKey: NSLocalizedString(@"Operation was unsuccessful.", nil),
     NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"The operation timed out.", nil),
     NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Have you tried turning it off and on again?", nil)

     */
    
    var userInfo: [AnyHashable: Any] {
        switch self {
        case .invalidRequest:
            return [NSLocalizedDescriptionKey: "Invalid request", NSLocalizedFailureReasonErrorKey: "Operation cannot be performed", NSLocalizedRecoverySuggestionErrorKey: "Please enter a valid request"]
        case .invalidDestination:
            return [NSLocalizedDescriptionKey: "Invalid destination", NSLocalizedFailureReasonErrorKey: "Operation cannot be performed", NSLocalizedRecoverySuggestionErrorKey: "Please enter a valid destination"]
        }
    }
    
}

class Error {
    
    class func invalidRequest() -> NSError {
        return _error(error: .invalidRequest)
    }
    
    class func invalidDestination() -> NSError {
        return _error(error: .invalidDestination)
    }
    
    fileprivate class func _error(error: ElvatorError) -> NSError {
        return NSError(domain: elevatorErrorDomain, code: error.code, userInfo: error.userInfo)
    }
    
}
