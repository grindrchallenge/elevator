//
//  ElevatorController.swift
//  Elevator
//
//  Created by Rohit Kotian on 2/25/17.
//
//

open class ElevatorController {
    
    fileprivate var _numberOfFloors: Int!

    // Variables
    fileprivate var _elevator = Elevator()
    
    // MARK: - Initializer
    
    fileprivate init() { }
    
    fileprivate init(numberOfFloors: Int) {
        _numberOfFloors = numberOfFloors
    }
    
}



// MARK: - Public Methods

public extension ElevatorController {
    
    public var currentFloor: Int {
        return _elevator.currentFloor;
    }
    
    public func request(floor: Int) throws {
        if !_isValidFloor(floor) {
            throw Error.invalidRequest()
        }
        if _elevator.state == .idle {
            _elevator.move(floor)
        } else {
            while _elevator.state != .idle {
                delay(after: 0.5, closure: {
                    try! self.request(floor: floor)
                })
            }
        }
    }
    
    public func destination(floor: Int) throws {
        if !_isValidFloor(floor) {
            throw Error.invalidDestination()
        }
        _elevator.request(floor)
    }
    
}

// MARK: - Private Helper Methods

fileprivate extension ElevatorController {
    
    fileprivate func _isValidFloor(_ floor: Int) -> Bool {
        return floor > 0 && floor <= _numberOfFloors
    }
    
}


// MARK: - Controller Builder

public struct ElevatorControllerBuilder {
    
    private var _elevatorController = ElevatorController()
    
    public func numberOfFloors(floors: Int) -> ElevatorControllerBuilder {
        _elevatorController._numberOfFloors = floors
        return self
    }
    
    public func build() -> ElevatorController {
        return _elevatorController
    }
    
    public var defaultController: ElevatorController {
        _elevatorController._numberOfFloors = 5
        return _elevatorController
    }
    
}

