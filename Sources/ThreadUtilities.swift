//
//  ThreadUtilities.swift
//  Elevator
//
//  Created by Rohit Kotian on 2/25/17.
//
//

import Foundation


/**
 Run a closure on queue after `n` seconds
 - Parameters:
 - queue: The queue to run on. Defaults to `main` queue.
 - after: The time in seconds
 - closure: The work to execute.
 */
public func delay(queue: DispatchQueue = DispatchQueue.main, after: Double, closure: @escaping ()->()) {
    let delay: DispatchTime = .now() + after
    queue.asyncAfter(deadline: delay, execute: closure)
}
