import XCTest
@testable import Elevator

class ElevatorTests: XCTestCase {
    
    func testExample() {
        let elevatorController = ElevatorControllerBuilder().defaultController
        
        try! elevatorController.request(floor: 5)
        let currentFloor = elevatorController.currentFloor
        
        XCTAssertEqual(currentFloor, 5)
    }
    
}
